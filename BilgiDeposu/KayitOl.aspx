﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KayitOl.aspx.cs" Inherits="BilgiDeposu.KayitOl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - Kayıt Ol</title>
    <uc1:Head runat="server" ID="Head" />
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/js/user-control.js"></script>
    <link href="assets/css/RdButtonSelect.css" rel="stylesheet" />
</head>

<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
    <form id="allInputsFormValidation" runat="server" defaultfocus="TxtUserName" defaultbutton="BtnRegister">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableViewState="true"></asp:ScriptManager>
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="Giris.aspx">Giriş Yap
                            </a>
                        </li>
                        <li>
                            <a href="SifremiUnuttum.aspx">Şifremi Unuttum
                            </a>
                        </li>
                        <li>
                            <a href="HesapAktivasyonu.aspx">Hesap Aktivasyonu
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="wrapper wrapper-full-page">
            <div class="full-page register-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg">
                <!-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
                <div class="content">
                    <div class="container">
                        <asp:Panel runat="server" ID="PnlForm">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="header-text">
                                    <br />
                                    <div id="logo" style="background-image: url(assets/img/logo-2.png); height: 120px; margin-left: -10px; margin-top: 5px; position: absolute; width: 120px; z-index: 9999;"></div>
                                    <br />
                                    <h4>
                                        <b>Kayıt Ol</b>
                                    </h4>
                                    <hr />
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-2">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="pe-7s-user"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5>
                                            <b>Ücretsiz Kayıt Ol</b>
                                        </h5>
                                        Bilgi Deposu'na ücretsiz olarak kayıt ol. Hesabın onaylandıktan sonra sana açılan derslere kolayca ulaş ve öğrenmeye başla.
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="pe-7s-graph1"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5>
                                            <b>İstatistik Verileri</b>
                                        </h5>
                                        Girdiğin sınavların sonuçlarına göre diğer öğrenciler ile kendini karşılaştır ve diğer tüm istatistiksel verileri gör, kendini daha çok geliştir.
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="pe-7s-mail"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5>
                                            <b>Aktivasyon Maili</b>
                                        </h5>
                                        Eğer hesabınızın doğrulanması için bir aktivasyon maili almadıysanız sağ üst köşedeki "Hesap Aktivasyonu" menüsünü kullanabilirsiniz.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-s1">
                                <div class="card card-plain">
                                    <div class="content">

                                        <div class="form-group">

                                            <input type="button" id="BtnUserNameControl" runat="server" title="Kullanıcı Adını Kontrol Et" style="background-color: transparent; background-image: url('assets/img/kullanici_kontrol.png'); background-repeat: no-repeat; border: none; border-radius: 5px; float: left; height: 40px; margin-right: 2%; text-align: center; width: 13%;" />
                                            <asp:TextBox ID="TxtUserName" runat="server" class="form-control" MaxLength="50" minlength="5" placeholder="Kullanıcı Adı" Width="85%" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqUserName" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Kullanıcı adı boş bırakılamaz!</b>" ValidationGroup="Register" ControlToValidate="TxtUserName" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FtUserName" runat="server" TargetControlID="TxtUserName" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " ValidChars="_ğüşıöçĞÜŞİÖÇ" />
                                        </div>

                                        <div class="form-group">
                                            <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" placeholder="Şifre" class="form-control" MaxLength="20" minlength="8" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqPassword" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifre Boş Bırakılamaz!</b>" ValidationGroup="Register" ControlToValidate="TxtPassword" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:PasswordStrength ID="PsPassword" Enabled="true" MinimumUpperCaseCharacters="1" MinimumNumericCharacters="1" StrengthStyles="dusuk; zayif; normal; iyi; cokiyi" TargetControlID="TxtPassword" PrefixText="Güvenlik: " TextStrengthDescriptions="Çok Düşük;Zayıf;Normal;İyi;Çok İyi" CalculationWeightings="50;15;15;20" DisplayPosition="RightSide" runat="server" StrengthIndicatorType="Text" PreferredPasswordLength="8" />
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FtPassword" runat="server" TargetControlID="TxtPassword" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" " />
                                            <asp:RegularExpressionValidator ID="RePassword" ControlToValidate="TxtPassword" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifreniz en az 1 büyük harf ve 1 rakam içermelidir.  </b>" ValidationExpression="((?=.*\d)(?=.*[A-Z_ĞÜŞİÖÇ]).{2,20})" ValidationGroup="Register" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </div>

                                        <div class="form-group">
                                            <asp:TextBox ID="TxtPasswordAgain" runat="server" TextMode="Password" placeholder="Şifre Tekrar" class="form-control" MaxLength="20" minlength="8" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqPasswordAgain" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifreyi tekrar girmelisiniz!</b>" ValidationGroup="Register" ControlToValidate="TxtPasswordAgain" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CvPasswordAgain" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Yazdığınız şifreler birbirini tutmuyor!</b>" ValidationGroup="Register" ControlToValidate="TxtPasswordAgain" ControlToCompare="TxtPassword" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:CompareValidator>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FtPasswordAgain" runat="server" TargetControlID="TxtPasswordAgain" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" " />
                                        </div>

                                        <div class="form-group">
                                            <asp:TextBox ID="TxtNameSurname" runat="server" class="form-control" placeholder="Ad Soyad" MaxLength="100" minlength="5" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqNameSurname" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Ad Soyad boş bırakılamaz!</b>" ValidationGroup="Register" ControlToValidate="TxtNameSurname" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="ReNameSurname" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b>Lütfen adınızı ve soyadınızı giriniz!</b><br><img src='assets/img/hata.png' width='24px' height='24px'><b> Sadece ad ve soyad arasında bir boşluk bırakın!</b>" ValidationExpression="^\S([a-z_ğüşıöçĞÜŞİÖÇ]*[A-Z_ğüşıöçĞÜŞİÖÇ]*)(\s.[a-z_ğüşıöçĞÜŞİÖÇ]*[A-Z_ğüşıöçĞÜŞİÖÇ]*)(\s.([a-z_ğüşıöçĞÜŞİÖÇ]*[A-Z_ğüşıöçĞÜŞİÖÇ]*)){0,1}\S$" ValidationGroup="Register" ControlToValidate="TxtNameSurname" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FtNameSurname" runat="server" TargetControlID="TxtNameSurname" FilterType="Custom, LowercaseLetters, UppercaseLetters" ValidChars="ğüşıöçĞÜŞİÖÇ " />
                                        </div>


                                        <div class="form-group">
                                            <asp:TextBox ID="TxtEmail" runat="server" class="form-control" placeholder="Email" MaxLength="100" email="true" ToolTip=" " ValidationGroup="Register" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqEmail" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Email boş bırakılamaz!</b>" ValidationGroup="Register" ControlToValidate="TxtEmail" Font-Size="10pt" ForeColor="White" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="ReEmail" runat="server" ErrorMessage="<br><img src='assets/img/hata.png' width='24px' height='24px'><b> Email adresi geçersiz!</b>" ValidationGroup="Register" ControlToValidate="TxtEmail" Font-Size="10pt" ForeColor="White" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FtEmail" runat="server" TargetControlID="TxtEmail" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" ğüşıöçĞÜŞİÖÇ" />
                                        </div>

                                        <div class="form-group">
                                            <div class="switch-field">
                                                <label style="text-align: left; width: 25%;">Yetki : </label>
                                                <asp:RadioButton ID="RdStudent" runat="server" GroupName="rol" />
                                                <label for="RdStudent" style="width: 32%;"><i class="fa fa-users" aria-hidden="true"></i>Öğrenci</label>
                                                <asp:RadioButton ID="RdAcademician" runat="server" GroupName="rol" />
                                                <label for="RdAcademician" style="float: none; width: 43%;"><i class="fa fa-user" aria-hidden="true"></i>Akademisyen</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <asp:Button ID="BtnRegister" runat="server" Text="Kayıt Ol" ValidationGroup="Register" class="btn btn-fill btn-neutral btn-wd" OnClick="BtnRegister_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </asp:Panel>
                    </div>
                </div>
                <footer class="footer footer-transparent">
                    <div class="container">
                        <p class="copyright pull-right" style="color: white;">
                            &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                        </p>
                    </div>
                </footer>
            </div>
        </div>

        <asp:Panel ID="PnlUserNameError" runat="server" Visible="false">
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-with-icon animated fadeInDown" role="alert" data-notify-position="bottom-center" style="bottom: 20px; display: inline-block; left: 0px; margin: 0px auto; position: fixed; right: 0px; transition: all 0.5s ease-in-out; z-index: 1031;">
                <span data-notify="icon" class="pe-7s-attention"></span><span data-notify="title"></span><span data-notify="message"><b>Hata</b> - Bu Kullanıcı Adı Daha Önce Kullanılmış !</span><a href="#" target="_blank" data-notify="url"></a>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlSelectionError" runat="server" Visible="false">
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-with-icon animated fadeInDown" role="alert" data-notify-position="bottom-center" style="bottom: 20px; display: inline-block; left: 0px; margin: 0px auto; position: fixed; right: 0px; transition: all 0.5s ease-in-out; z-index: 1031;">
                <span data-notify="icon" class="pe-7s-attention"></span><span data-notify="title"></span><span data-notify="message"><b>Hata</b> - Lütfen Öğrenci ve Akademisyen seçimi yapınız.!</span><a href="#" target="_blank" data-notify="url"></a>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlEmailError" runat="server" Visible="false">
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-with-icon animated fadeInDown" role="alert" data-notify-position="bottom-center" style="bottom: 20px; display: inline-block; left: 0px; margin: 0px auto; position: fixed; right: 0px; transition: all 0.5s ease-in-out; z-index: 1031;">
                <span data-notify="icon" class="pe-7s-attention"></span><span data-notify="title"></span><span data-notify="message"><b>Hata</b> - Bu Email Adresi Daha Önce Kullanılmış !</span><a href="#" target="_blank" data-notify="url"></a>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlInputError" runat="server" Visible="false">
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-with-icon animated fadeInDown" role="alert" data-notify-position="bottom-center" style="bottom: 20px; display: inline-block; left: 0px; margin: 0px auto; position: fixed; right: 0px; transition: all 0.5s ease-in-out; z-index: 1031;">
                <span data-notify="icon" class="pe-7s-attention"></span><span data-notify="title"></span><span data-notify="message"><b>Hata</b> - Lütfen Belirtilen Karakterler Arasında Karakter Giriniz !</span><a href="#" target="_blank" data-notify="url"></a>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlRegisterSuccess" runat="server" Visible="false">
            <div class="sweet-container">
                <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                    <div class="icon error" style="display: none;">
                        <span class="x-mark">
                            <span class="line left"></span>
                            <span class="line right"></span>
                        </span>
                    </div>
                    <div class="icon warning" style="display: none;">
                        <span class="body"></span><span class="dot"></span>
                    </div>
                    <div class="icon info" style="display: none;"></div>
                    <div class="icon success animate" style="display: block;">
                        <span class="line tip animate-success-tip"></span>
                        <span class="line long animate-success-long"></span>
                        <div class="placeholder"></div>
                        <div class="fix"></div>
                    </div>
                    <img class="sweet-image" style="display: none;">
                    <h4>Tebrikler</h4>
                    <div class="sweet-content">
                        <p style="color: green; font-size: 16px;">
                            Hesabınız Oluşturuldu.<br />
                            <span style="color: #1C466C; font-size: 13px;">
                                <br />
                                Lütfen Hesabınızı Email Adresinize Gelen Mesaj İle Doğrulayın!
                            </span>
                        </p>
                    </div>
                    <hr class="sweet-spacer" style="display: block;">
                    <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Sayfası</a><br />
                    <a class="btn btn-bilgideposu" style="width: 200px;" href="HesapAktivasyonu.aspx">Aktivasyon Maili Gelmedi!</a>
                </div>
            </div>
        </asp:Panel>


    </form>
</body>

<uc1:Footer runat="server" ID="Footer" />
<script type="text/javascript">
    $().ready(function () {

        $('#registerFormValidation').validate();
        $('#loginFormValidation').validate();
        $('#allInputsFormValidation').validate();
    });
</script>
<script type="text/javascript">
    $().ready(function () {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        },
            1000);
    });
</script>
</html>
