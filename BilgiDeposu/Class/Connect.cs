﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BilgiDeposu.DataModel;

namespace BilgiDeposu.Class
{
    public class Connect : System.Web.UI.Page
    {
        public static BilgiDeposuDataModel db = new BilgiDeposuDataModel();

        public static User GetCurrentUser()
        {
            return HttpContext.Current.Session["User"] as User;
        }

        public string LoginAlert(string message)
        {
            return
                "<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b>" +
                message + "</b></div>";
        }

    }
}