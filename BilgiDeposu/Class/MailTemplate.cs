﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilgiDeposu.Class
{
    public class MailTemplate
    {
        public string Template(string nameSurname, string title, string message)
        {
            var template = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns = 'http://www.w3.org/1999/xhtml'><head><meta http - equiv = 'Content-Type' content = 'text/html; charset=utf-8'/></head><body style ='font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif'><table width = '320px' style = 'margin-left:auto;margin-right:auto;'>" +
                         "<tr><td style = 'background:url(http://hanmurat.com/bilgideposu/emailBanner.png);width:320px;height:81px;background-repeat:no-repeat;'></td></tr><tr><td align = 'center' valign = 'middle' style='line-height:15px;'><hr/><span style='font-size:24px;font-weight:bold;color:#0E3048;'><br /><img src='http://hanmurat.com/bilgideposu/email.png' width='45' height='45' /><br /><br />   " + title + "</span></td></tr><tr><td align = 'center' valign = 'middle'><h2 style = 'color:#0E3048;'> Merhaba <span style ='color:#0797EA;'>" + nameSurname + "</span>,</h2></td></tr>" +
                         "<tr><td align = 'center' valign = 'middle' style = 'color:#091E56;font-size:18px;'><p style='text-align:justify;'>" + message + "</p><br/><a href = 'http://hanmurat.com' style='color:#fff;font-weight:bold;font-family:Tahoma, Geneva, sans-serif;font-size:14px;background-color:#0797EA;text-decoration:none;height:30px;width:70%;border-radius:50px;padding:15px;'>Giriş Yap</a><br/><br/></td></tr>" +
                         "<tr><td style = 'background:url(http://hanmurat.com/bilgideposu/emailFooter.png);width:320px;height:85px;background-repeat:no-repeat;'></td></tr></table></body></html>";

            return template;
        }
    }
}