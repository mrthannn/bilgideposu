﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.DataModel;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;

namespace BilgiDeposu
{
    public partial class SifremiUnuttum : Connect
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
            }


        }

        protected void BtnForgotPassword_Click(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                return;

            }
            else
            {
                string email = TxtEmail.Text.Trim();
                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                if (email.Length > 150)
                {
                    PnlInputError.Visible = true;
                    PnlEmailNotFound.Visible = false;
                    PnlSuccess.Visible = false;
                    PnlAgain.Visible = false;
                }
                else
                {
                    var emailControl = db.User.Where(k => k.Email == email).ToList();

                    if (emailControl.Count < 1)
                    {
                        PnlForm.Visible = false;
                        PnlInputError.Visible = false;
                        PnlEmailNotFound.Visible = true;
                        PnlSuccess.Visible = false;
                        PnlAgain.Visible = false;
                    }
                    else
                    {
                        var lastRequestControl =
                            db.LastRequest.FirstOrDefault(
                                k => k.User.Email == email && k.Process == LastRequestType.ForgotPassword);

                        if (lastRequestControl != null)
                        {

                            var date = lastRequestControl.Date;
                            var diff = Convert.ToInt32(DateTime.Now.Subtract(date).TotalMinutes);
                            if (diff <= 60)
                            {
                                LblMail.Text = lastRequestControl.User.Email;
                                LblMinute.Text = diff.ToString();
                                LblDiffMinute.Text = (60 - diff).ToString();
                                PnlAgain.Visible = true;
                            }
                            else
                            {
                                string pin = Encryption.Encode(Encryption.Encode(Encryption.GenerateCode()));
                                SendMail(lastRequestControl.User.NameSurname, lastRequestControl.User.Email, lastRequestControl.User.UserName,pin);

                               

                                var user = db.User.FirstOrDefault(k => k.Email == email);
                                var lastRequest = db.LastRequest.FirstOrDefault(k => k.User.UserName == user.UserName && k.Process == LastRequestType.ForgotPassword);
                                lastRequest.Date = DateTime.Now;
                                lastRequest.VerificationCode = Encryption.Decode(Encryption.Decode(pin));
                                db.SaveChanges();
                            }
                            

                        }
                        else
                        {
                            string pin = Encryption.Encode(Encryption.Encode(Encryption.GenerateCode()));
                            var user = db.User.FirstOrDefault(k => k.Email == email);
                            if (user != null) SendMail(user.NameSurname, user.Email, user.UserName,pin);

                            
                            LastRequest lastRequest = new LastRequest();
                            lastRequest.User =
                                db.User.FirstOrDefault(
                                    k => k.UserName == user.UserName);
                            lastRequest.Date = DateTime.Now;
                            lastRequest.Process = LastRequestType.ForgotPassword;
                            lastRequest.VerificationCode = Encryption.Decode(Encryption.Decode(pin));
                            db.LastRequest.Add(lastRequest);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }


        public void SendMail(string nameSurname, string email, string userName,string pin)
        {
            new SendMail().MailGonder("mrthan0227@gmail.com", "Bilgi Deposu Şifre Yenileme",
                new MailTemplate().Template(nameSurname,
                    "Şifre Yenileme",
                    "Bilgi Deposu şifre yenileme talebiniz için bu maili almış bulunmaktasınız. Şifrenizi yenilemek için <b>Yeni Şifre Al</b> butonuna tıklayın.<br /><br />Faydalı olması dileklerimizle.<br><br><p style='text-align:center;'><a href=\"http://localhost:58953/SifreYenile.aspx?Code=" +
                    Encryption.Encode(Encryption.Encode(email.ToString())) + "&Verification=" +
                    pin.ToString() +
                    "/\" style='color:#fff;font-weight:bold;font-family:Tahoma, Geneva, sans-serif;font-size:14px;background-color:#0797EA;text-decoration:none;height:30px;width:70%;border-radius:50px;padding:15px;'>Yeni Şifre Al</a></p>"));

            PnlForm.Visible = false;
            PnlSuccess.Visible = true;
            PnlInputError.Visible = false;
            PnlEmailNotFound.Visible = false;
            PnlAgain.Visible = false;
        }

        private void Captcha(int height, int weight, string fonts, int punto, int x, int y, string backgroundImage)
        {
            Bitmap bmp = new Bitmap(height, weight);
            Graphics g = Graphics.FromImage(bmp);
            Font font = new Font(fonts, punto);
            Random r = new Random();
            int sayi = r.Next(100000, 999999);
            ViewState["captcha"] = sayi;
            string yazi = "\"" + sayi.ToString() + "\"";
            System.Drawing.Image img = System.Drawing.Image.FromFile(backgroundImage);
            g.DrawImage(img, 0, 0);
            g.RotateTransform(0);
            g.DrawString(yazi.ToString(), font, Brushes.Gray, x, y);
            g.CompositingQuality = CompositingQuality.HighQuality;
            bmp.Save(Server.MapPath("assets/img/captcha.png"), ImageFormat.Png);
        }

        protected void CaptchaControl(object source, ServerValidateEventArgs args)
        {
            if (ViewState["captcha"] != null)
            {
                if (TxtCaptcha.Text != ViewState["captcha"].ToString())
                {
                    Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                    args.IsValid = false;
                    return;
                }
            }
            else
            {
                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                args.IsValid = false;
                return;
            }

        }
    }
}