﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Giris.aspx.cs" Inherits="BilgiDeposu.Ogrenci.Giris" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Light Bootstrap Dashboard PRO by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

   
    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'/>
    <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<form id="form1" runat="server">
<div class="wrapper">
<div class="sidebar" data-color="azure" data-image="../assets/img/full-screen-image-3.jpg">
    <!--

    Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
    Tip 2: you can also add an image using data-image tag

    -->

    <div class="logo">
        <a href="#" class="logo-text">
            <b>Bilgi </b>Deposu
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="#" class="logo-text">BD
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="../assets/img/default-avatar.png" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#" class="collapsed"><span style="text-transform:capitalize;">
                        <asp:Label ID="LblAdSoyad" runat="server" Text=""></asp:Label></span>
                    <%--<b class="caret"></b>--%>
                </a>
                <%--<div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li><a href="#">Profilim</a></li>
                            <li><a href="#">Profilimi Düzenle</a></li>
                        </ul>
                    </div>--%>
            </div>
        </div>

        <ul class="nav">
            <li class="active">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <p>ANASAYFA</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-id-card"></i>
                    <p>Profilim</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-book"></i>
                    <p>Aktif Dersler</p>
                </a>
            </li>
            <li>
            <li>
                <a href="#">
                    <i class="fa fa-check-square-o"></i>
                    <p>Sınavlar</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-line-chart"></i>
                    <p>Sınav Sonuçlarım</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <p>Ödevlerim</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#componentsExamples">
                    <i class="fa fa-angle-double-down"></i>
                    <p>
                        Diğer
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples">
                    <ul class="nav">
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Akademisyenler</a></li>
                        <li><a href="#">Öğrenciler</a></li>
                        <li><a href="#">Tüm Dersler</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="main-panel">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <div id="minimizeSidebar" class="btn btn-info btn-fill btn-round btn-icon">
                <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                <i class="fa fa-navicon visible-on-sidebar-mini"></i>
            </div>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Öğrenci Paneli</a>
        </div>
        <div class="collapse navbar-collapse">

            <div class="navbar-form navbar-left navbar-search-form" role="search">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" value="" class="form-control" placeholder="Ne Aramıştınız?"/>
                </div>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <%--<li>
							<a href="charts.html">
								<i class="fa fa-line-chart"></i>
								<p>Stats</p>
							</a>
						</li>--%>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="notification">5</span>
                        <p>
                            Mesajlar
                        </p>
                    </a>

                </li>
                <li class="dropdown dropdown-with-icons">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-lock"></i>
                        <p>
                            Kilitle
                        </p>
                    </a>

                </li>

                <li class="dropdown dropdown-with-icons">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-sign-out"></i>
                        <p>
                            Çıkış Yap
                        </p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="content">
    <div class="container-fluid">
        <h5>Aktif Dersleriniz</h5>
        <div class="row">

            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-globe" style="font-size: 50px; color: #F3BB45;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Routing & Switching 1</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Doç. Dr. Resul Daş
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-book" style="font-size: 50px; color: #F3BB45;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Yazılım Kalite ve Güvence Testi</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr/>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Doç. Dr. Resul DAŞ
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-desktop" style="font-size: 50px; color: #7AC29A;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Web Tasarım ve Programlama</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Doç. Dr. Erkan TANYILDIZI
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-database" style="font-size: 50px; color: #DD0330;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Veritabanı Yönetim Sistemleri</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Doç. Dr. Murat KARABATAK
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-key" style="font-size: 50px; color: #68B3C8;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Mikroişlemciler ve Programlama</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Prof. Dr. İbrahim TÜRKOĞLU
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="text-center">
                                    <i class="fa fa-book" style="font-size: 50px; color: #DD0330;"></i>
                                </div>
                            </div>
                            <div class="col-xs-10 text-center">
                                <div class="numbers">
                                    <p>Veri Madenciliği</p>
                                    <span style="color: green;">Uygun <i class="fa fa-play-circle" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-user-o"></i>Doç. Dr. Murat KARABATAK
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center"><a href="#" class="btn btn-round btn-wd">Tümünü Gör...</a></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>Son Bildirimler</h5>
                <div class="alert alert-info">

                    <span><b>Bildirim - </b>Bildirim Açıklaması</span>
                </div>
                <div class="alert alert-success">

                    <span><b>Bildirim - </b>Bildirim Açıklaması</span>
                </div>
                <div class="alert alert-warning">

                    <span><b>Bildirim - </b>Bildirim Açıklaması</span>
                </div>
                <div class="alert alert-danger">

                    <span><b>Bildirim - </b>Bildirim Açıklaması</span>
                </div>
                <div class="text-center"><a href="#" class="btn btn-round btn-wd">Tümünü Gör...</a></div>
            </div>



        </div>



        <div class="row">
        </div>



    </div>
</div>


<footer class="footer">
    <div class="container-fluid">

        <p class="copyright pull-right">
            &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
        </p>
    </div>
</footer>

</div>
</div>

</form>
</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="../assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="../assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="../assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="../assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="../assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="../assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="../assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="../assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="../assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="../assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="../assets/js/light-bootstrap-dashboard.js"></script>

<!--   Sharrre Library    -->
<script src="../assets/js/jquery.sharrre.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>



</html>