﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;

namespace BilgiDeposu
{
    public partial class Giris : Connect
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Counter"] = 0;
            }

            if (GetCurrentUser() != null)
            {
                if (GetCurrentUser().UserType == UserType.Admin)
                {
                    Response.Redirect("~/Admin/Default.aspx");
                }
                else if (GetCurrentUser().UserType == UserType.Academician)
                {
                    Response.Redirect("~/Akademisyen/Default.aspx");
                }
                else if (GetCurrentUser().UserType == UserType.Student)
                {
                    Response.Redirect("~/Ogrenci/Default.aspx");
                }
            }
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            var password = Encryption.Sha256Code(TxtPassword.Text);
            var userLogin = db.User.FirstOrDefault(k => k.Password == password &&
                                                    (k.Email == TxtUserName.Text || k.UserName == TxtUserName.Text));

            if (userLogin == null)
            {
                Session["Counter"] = Convert.ToInt32(Session["sayac"]) + 1;
                LblInfo.Text = LoginAlert("Kullanıcı Adı veya Şifre Hatalı!");
                if (Convert.ToInt32(Session["Counter"]) > 4)
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Session["User"] = userLogin;

                if (userLogin.UserType == UserType.Admin)
                {
                    Response.Redirect("~/Admin/Default.aspx");
                }
                else if (userLogin.UserType == UserType.Academician)
                {
                    Response.Redirect("~/Akademisyen/Default.aspx");
                }
                else if (userLogin.UserType == UserType.Student)
                {
                    Response.Redirect("~/Ogrenci/Default.aspx");
                }
            }
        }
    }
}