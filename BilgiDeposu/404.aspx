﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="BilgiDeposu._404" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - 404 Hatası!</title>
    <uc1:Head runat="server" ID="Head"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet" type="text/css"/>
    <link href="404/animate.css" rel="stylesheet"/>
    <link href="404/custom.css" rel="stylesheet"/>

    <style>
        .login-page > .content, .lock-page > .content { padding-top: 10vh; }
    </style>
</head>
<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
<form id="Form1" runat="server">
    <nav class="navbar navbar-transparent navbar-absolute" style="background-color: transparent;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="KayitOl.aspx">
                            Kayıt Ol
                        </a>
                    </li>
                    <li>
                        <a href="Giris.aspx">
                            Giriş Yap
                        </a>
                    </li>
                    <li>
                        <a href="HesapAktivasyonu.aspx">
                            Hesap Aktivasyonu
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg" style="-moz-background-size: cover; -o-background-size: cover; -webkit-background-size: cover; background: url(assets/img/full-screen-image-3.jpg) no-repeat center center fixed; background-size: cover; height: auto; left: 0; min-height: 100%; min-width: 100%; top: 0; width: 100%;">
            <!-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div>
                    <div>
                        <section>
                            <div style="color: white;">
                                <div class="row row1">
                                    <div class="col-md-12">
                                        <h3 class="center capital f1 wow fadeInLeft" data-wow-duration="2s">HATA!</h3>
                                        <h1 id="error" class="center wow fadeInRight" data-wow-duration="2s">0</h1>
                                        <p class="center wow bounceIn" data-wow-delay="2s">Bir Hata Meydana Geldi!</p>
                                    </div>
                                </div>
                                <div class="row" style="color: black;">
                                    <div class="col-md-12">
                                        <div id="cflask-holder" class="wow fadeIn" data-wow-delay="2800ms">
                                            <span class="wow tada " data-wow-delay="3000ms">
                                                <i class="fa fa-flask fa-5x flask wow flip" data-wow-delay="3300ms"></i>
                                                <i id="b1" class="bubble" style="margin-left: 6px;"></i>
                                                <i id="b2" class="bubble" style="margin-left: 6px;"></i>
                                                <i id="b3" class="bubble" style="margin-left: 6px;"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <footer class="footer footer-transparent">
                <div class="container">
                    <p class="copyright pull-right" style="color: white;">
                        &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                    </p>
                </div>
            </footer>
        </div>
    </div>
</form>
</body>
<uc1:Footer runat="server" ID="Footer"/>
<script src="404/countUp.js"></script>
<script src="404/wow.js"></script>
<script type="text/javascript">
    "use strict";
    var count = new countUp("error", 0, 404, 0, 3);

    window.onload = function() {
        // fire animation
        count.start();
    };
</script>
<!--Initiating the Wow Script-->
<script>
    "use strict";
    var wow = new WOW(
        {
            animateClass: 'animated',
            offset: 100
        }
    );
    wow.init();
</script>
</html>