﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BilgiDeposu.Enums
{
    public enum UserType
    {
        [Description("Admin")]
        Admin = 1,

        [Description("Akademisyen")]
        Academician = 2,

        [Description("Öğrenci")]
        Student = 3
    }
}