﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BilgiDeposu.Enums
{
    public enum LastRequestType
    {
        [Description("Mail Aktivasyon")]
        MailActivation = 1,

        [Description("Şifremi Unuttum")]
        ForgotPassword = 2
    }
}