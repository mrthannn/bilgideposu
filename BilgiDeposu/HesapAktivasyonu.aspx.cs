﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;
using Image = System.Drawing.Image;

namespace BilgiDeposu
{
    public partial class HesapAktivasyonu : Connect
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
        }

        protected void BtnActivation_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
            }
            else
            {
                var email = TxtEmail.Text.Trim();
                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                if (email.Length > 150)
                {
                    PnlInputError.Visible = true;
                    PnlMailError.Visible = false;
                    PnlAlreadyActive.Visible = false;
                    PnlSuccess.Visible = false;
                }
                else
                {
                    var emailControl = db.User.FirstOrDefault(k => k.Email == email);

                    if (emailControl == null)
                    {
                        PnlInputError.Visible = false;
                        PnlMailError.Visible = true;
                        PnlAlreadyActive.Visible = false;
                        PnlSuccess.Visible = false;
                    }
                    else
                    {
                        var emailVerification = db.User.FirstOrDefault(k => k.EmailVerification && k.Email == email);

                        if (emailVerification != null)
                        {
                            PnlAlreadyActive.Visible = true;
                            PnlInputError.Visible = false;
                            PnlMailError.Visible = false;
                            PnlSuccess.Visible = false;
                        }
                        else
                        {
                            var lastRequestControl =
                                db.LastRequest.FirstOrDefault(
                                    k => k.User.Email == email && k.Process == LastRequestType.MailActivation);

                            var date = lastRequestControl.Date;
                            var diff = Convert.ToInt32(DateTime.Now.Subtract(date).TotalMinutes);

                            if (diff <= 60)
                            {
                                LblMail.Text = lastRequestControl.User.Email;
                                LblMinute.Text = diff.ToString();
                                LblDiffMinute.Text = (60 - diff).ToString();
                                PnlAgainError.Visible = true;
                            }
                            else
                            {
                                var pin = Encryption.Encode(Encryption.Encode(Encryption.GenerateCode()));

                                SendMail(lastRequestControl.User.NameSurname, lastRequestControl.User.Email,
                                    lastRequestControl.User.UserName, pin);


                                var user = db.User.FirstOrDefault(k => k.Email == email);
                                var lastRequest =
                                    db.LastRequest.FirstOrDefault(k => k.User.UserName == user.UserName &&
                                                                       k.Process == LastRequestType.MailActivation);
                                lastRequest.Date = DateTime.Now;
                                lastRequest.VerificationCode = Encryption.Decode(Encryption.Decode(pin));
                                db.SaveChanges();

                                PnlForm.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        public void SendMail(string nameSurname, string email, string userName, string pin)
        {
            new SendMail().MailGonder("mrthan0227@gmail.com", "Bilgi Deposu Hesap Aktivasyonu",
                new MailTemplate().Template(nameSurname,
                    "Hesap Aktivasyonu",
                    "Bilgi Deposu'na kayıt olduğunuz için teşekkür ederiz. Bilgi Deposunu kullanabilmeniz için bu email adresinin size ait olduğundan emin olmamız gerekiyor. Hesabı doğrulamak için aşağıdaki bağlantıyı tıklayın.<br /><br />Faydalı olması dileklerimizle.<br><br><p style='text-align:center;'><a href=\"http://localhost:58953/HesabiDogrula.aspx?Code=" +
                    Encryption.Encode(Encryption.Encode(email)) + "&Verification=" +
                    pin +
                    "/\" style='color:#fff;font-weight:bold;font-family:Tahoma, Geneva, sans-serif;font-size:14px;background-color:#0797EA;text-decoration:none;height:30px;width:70%;border-radius:50px;padding:15px;'>Hesabımı Doğrula</a></p>"));


            PnlSuccess.Visible = true;
            PnlInputError.Visible = false;
            PnlMailError.Visible = false;
        }

        private void Captcha(int high, int weight, string fonts, int punto, int x, int y, string backgroundImage)
        {
            var bmp = new Bitmap(high, weight);
            var g = Graphics.FromImage(bmp);
            var font = new Font(fonts, punto);
            var r = new Random();
            var sayi = r.Next(100000, 999999);
            ViewState["captcha"] = sayi;
            var yazi = "\"" + sayi + "\"";
            var img = Image.FromFile(backgroundImage);
            g.DrawImage(img, 0, 0);
            g.RotateTransform(0);
            g.DrawString(yazi, font, Brushes.Gray, x, y);
            g.CompositingQuality = CompositingQuality.HighQuality;
            bmp.Save(Server.MapPath("assets/img/captcha.png"), ImageFormat.Png);
        }

        protected void CaptchaControl(object source, ServerValidateEventArgs args)
        {
            if (ViewState["captcha"] != null)
            {
                if (TxtCaptcha.Text != ViewState["captcha"].ToString())
                {
                    Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                    args.IsValid = false;
                }
            }
            else
            {
                Captcha(100, 40, "Arial", 14, 6, 9, Server.MapPath("assets/img/captchaBg.png"));
                args.IsValid = false;
            }
        }
    }
}