﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Giris.aspx.cs" Inherits="BilgiDeposu.Giris" %>

<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - Giriş Yap</title>
    <uc1:Head runat="server" ID="Head"/>
</head>
<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
<form id="allInputsFormValidation" runat="server">
    <nav class="navbar navbar-transparent navbar-absolute" style="background-color: transparent;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="KayitOl.aspx">
                            Kayıt Ol
                        </a>
                    </li>
                    <li>
                        <a href="SifremiUnuttum.aspx">
                            Şifremi Unuttum
                        </a>
                    </li>
                    <li>
                        <a href="HesapAktivasyonu.aspx">
                            Hesap Aktivasyonu
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg">
            <!-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <div id="logo" class="card card-hidden" style="background-color: transparent; background-image: url(assets/img/logo-1.png); box-shadow: 0 0px 0px 0px white; height: 118px; margin-left: -5px; margin-top: -20px; position: absolute; width: 118px; z-index: 9999;"></div>
                            <!-- if you want to have the card without animation please remove the ".card-hidden" class -->
                            <div class="card card-hidden">
                                <div class="header text-right" style="margin-right: 40px;">Giriş Yap</div>
                                <div class="content">
                                    <div class="form-group">
                                        <asp:TextBox ID="TxtUserName" class="form-control" placeholder="Kullanıcı Adı veya Email Adresi" MaxLength="100" autocomplete="off" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RqEmail" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Kullanıcı adı veya email alanı boş bırakılamaz!</b></div>" ValidationGroup="Login" ControlToValidate="TxtUserName" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" placeholder="Şifre" class="form-control" MaxLength="25" autocomplete="off" aria-required="true"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RqPassword" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifre boş bırakılamaz!</b></div>" ValidationGroup="Login" ControlToValidate="TxtPassword" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 20px;">
                                        <label class="checkbox">
                                            <asp:CheckBox ID="ChkRememberMe" runat="server" data-toggle="checkbox"/>
                                            <span style="text-transform: capitalize;">Beni Hatırla </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="footer text-center" style="margin-top: -10px;">
                                    <asp:Button ID="BtnLogin" runat="server" Text="Giriş Yap" ValidationGroup="Login" class="btn btn-bilgideposu" OnClick="BtnLogin_Click"/>
                                    <asp:Label ID="LblInfo" runat="server" Text="" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer footer-transparent">
                <div class="container">
                    <p class="copyright pull-right" style="color: white;">
                        &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                    </p>
                </div>
            </footer>
        </div>
    </div>
</form>
</body>
<uc1:Footer runat="server" ID="Footer"/>
<script type="text/javascript">
    $().ready(function() {

        $('#registerFormValidation').validate();
        $('#loginFormValidation').validate();
        $('#allInputsFormValidation').validate();
    });
</script>

<script type="text/javascript">
    $().ready(function() {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function() {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
                $('#logo').removeClass('card-hidden');
            },
            700);
    });
</script>
</html>