﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BilgiDeposu.Enums;

namespace BilgiDeposu.DataModel
{
    public class LastRequest
    {
        public int Id { get; set; }
        
        public virtual User User { get; set; }
        
        public DateTime Date { get; set; }
        
        public string VerificationCode { get; set; }

        public LastRequestType Process { get; set; }

    }
}