﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BilgiDeposu.Enums;

namespace BilgiDeposu.DataModel
{
    public class User
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(100)]
        public string Password { get; set; }

        [StringLength(150)]
        public string NameSurname { get; set; }

        public UserType UserType { get; set; }

        [StringLength(120)]
        public string Email { get; set; }

        public string Image { get; set; }

        public bool EmailVerification { get; set; }

        public bool AccountVerification { get; set; }

        public DateTime DateOfRegistration { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        public bool Delete { get; set; }
    }
}