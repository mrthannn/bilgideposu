namespace BilgiDeposu.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BilgiDeposuDataModel : DbContext
    {
        public BilgiDeposuDataModel()
            : base("name=BilgiDeposuDataModel")
        {
        }

        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<LastRequest> LastRequest { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
