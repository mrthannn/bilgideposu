﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.DataModel;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;

namespace BilgiDeposu
{
    public partial class KayitOl : Connect
    {
        UserType UserType;

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void BtnRegister_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            string nameSurname = TxtNameSurname.Text.Trim();
            string userName = TxtUserName.Text.Trim();
            string password = TxtPassword.Text.Trim();
            string email = TxtEmail.Text.Trim();

            if ((RdStudent.Checked == false && RdAcademician.Checked == false) ||
                (RdStudent.Checked == true && RdAcademician.Checked == true))
            {
                PnlSelectionError.Visible = true;
                PnlInputError.Visible = false;
                PnlUserNameError.Visible = false;
                PnlEmailError.Visible = false;
                PnlRegisterSuccess.Visible = false;
            }
            else
            {
                if ((nameSurname.Length > 100 || nameSurname.Length < 5) || (email.Length > 100) ||
                    (password.Length < 8 || password.Length > 20) || (userName.Length < 5 || userName.Length > 50))
                {
                    PnlInputError.Visible = true;
                    PnlUserNameError.Visible = false;
                    PnlEmailError.Visible = false;
                    PnlRegisterSuccess.Visible = false;
                    PnlSelectionError.Visible = false;
                }
                else
                {
                    if (RdStudent.Checked == true && RdAcademician.Checked == false)
                        UserType = UserType.Student;
                    else if (RdStudent.Checked == false && RdAcademician.Checked == true)
                        UserType = UserType.Academician;

                    var userNameControl = db.User.Where(k => k.UserName == userName).ToList();

                    if (userNameControl.Count >= 1)
                    {
                        PnlInputError.Visible = false;
                        PnlUserNameError.Visible = true;
                        PnlEmailError.Visible = false;
                        PnlRegisterSuccess.Visible = false;
                        PnlSelectionError.Visible = false;
                    }
                    else
                    {
                        var emailControl = db.User.Where(k => k.Email == email).ToList();
                        if (emailControl.Count >= 1)
                        {
                            PnlInputError.Visible = false;
                            PnlUserNameError.Visible = false;
                            PnlEmailError.Visible = true;
                            PnlRegisterSuccess.Visible = false;
                            PnlSelectionError.Visible = false;
                        }
                        else
                        {
                            string pin = Encryption.Encode(Encryption.Encode(Encryption.GenerateCode()));
                            //*** Yeni Kullanıcı Kaydetme ***
                            User addUser = new User();
                            addUser.NameSurname = nameSurname;
                            addUser.Email = email;
                            addUser.Password = Encryption.Sha256Code(password);
                            addUser.UserName = userName;
                            addUser.UserType = UserType;
                            addUser.EmailVerification = false;
                            addUser.AccountVerification = false;
                            addUser.DateOfRegistration = DateTime.Now;
                            addUser.Delete = false;
                            db.User.Add(addUser);
                            db.SaveChanges();

                            LastRequest lastRequest = new LastRequest();
                            var user = db.User.FirstOrDefault(k => k.UserName == userName);
                            lastRequest.User = user;
                            lastRequest.Date = DateTime.Now;
                            lastRequest.Process = LastRequestType.MailActivation;
                            lastRequest.VerificationCode = Encryption.Decode(Encryption.Decode(pin));
                            db.LastRequest.Add(lastRequest);
                            db.SaveChanges();

                            //var datediff =
                            //    db.LastRequest.FirstOrDefault(k => k.User == GetCurrentUser() &&
                            //                              k.Process == LastRequestType.MailActivation);

                            //TimeSpan minute = datediff.Date.Subtract(DateTime.Now);

                            new SendMail().MailGonder("mrthan0227@gmail.com", "Bilgi Deposu Hesap Aktivasyonu",
                                new MailTemplate().Template(nameSurname, "Hesap Doğrulama",
                                    "Başarılı bir şekilde sistemimize kayıt oldunuz. Hesabınızın doğruluğundan emin olmamız için <b>Hesabımı Doğrula</b> butonuna tıklayın.<br /><br />Faydalı olması dileklerimizle.<br><br><p style='text-align:center;'><a href=\"http://localhost:11407/HesabiDogrula.aspx?Code=" +
                                    Encryption.Encode(Encryption.Encode(email.ToString())) + "&Verification=" +
                                    pin.ToString() +
                                    "/\" style='color:#fff;font-weight:bold;font-family:Tahoma, Geneva, sans-serif;font-size:14px;background-color:#0797EA;text-decoration:none;height:30px;width:70%;border-radius:50px;padding:15px;'>Hesabımı Doğrula</a></p>"));

                            PnlInputError.Visible = false;
                            PnlUserNameError.Visible = false;
                            PnlEmailError.Visible = false;
                            PnlRegisterSuccess.Visible = true;
                            PnlSelectionError.Visible = false;
                            PnlForm.Visible = false;
                        }
                    }
                }
            }
        }

        [WebMethod]
        public static string UserNameControl(string userName)
        {
            var parameter = userName;
            var control = db.User.Where(k => k.UserName == parameter.Trim()).ToList();
            int userCount = control.Count;
            if (userCount >= 1)
            {
                return "kullaniciVar";
            }
            else
            {
                return "kullaniciYok";
            }
        }
    }
}