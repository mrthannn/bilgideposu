﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SifreYenile.aspx.cs" Inherits="BilgiDeposu.SifreYenile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - Şifre Yenile</title>
    <uc1:Head runat="server" ID="Head" />
    <style>
        @import url('https://fonts.googleapis.com/css?family=Orbitron');
        .error { margin-top: 10px; }
    </style>
</head>
<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
<form id="allInputsFormValidation" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableViewState="true"></asp:ScriptManager>
    <nav class="navbar navbar-transparent navbar-absolute" style="background-color: transparent;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="KayitOl.aspx">
                            Kayıt Ol
                        </a>
                    </li>
                    <li>
                        <a href="Giris.aspx">
                            Giriş Yap
                        </a>
                    </li>
                    <li>
                        <a href="SifremiUnuttum.aspx">
                            Şifremi Unuttum
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg">
            <!-- sayfanın arka plan renkleri için bunlar kulanılabilir; data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <asp:Panel runat="server" ID="PnlForm">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" style="margin-top: -40px;">
                            <div id="logo" class="card card-hidden" style="background-color: transparent; background-image: url(assets/img/logo-1.png); box-shadow: 0 0px 0px 0px white; height: 118px; margin-left: -5px; margin-top: -20px; position: absolute; width: 118px; z-index: 9999;"></div>
                            <!-- animasyonu silmek için ".card-hidden" silinecek -->
                            <div class="card card-hidden">
                                <div class="header text-right">
                                    <asp:Label ID="LblTitle" runat="server" Text="Şifre Al" Font-Size="11pt"></asp:Label>
                                </div>
                                <div class="content">
                                    <div class="form-group">
                                        <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" placeholder="Şifre" class="form-control" MaxLength="20" minlength="8" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RqPassword" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifre Boş Bırakılamaz!</b></div>" ValidationGroup="UpdatePassword" ControlToValidate="TxtPassword" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <ajaxToolkit:PasswordStrength ID="PsPassword" Enabled="true" MinimumUpperCaseCharacters="1" MinimumNumericCharacters="1" StrengthStyles="dusuk; zayif; normal; iyi; cokiyi" TargetControlID="TxtPassword" PrefixText="Güvenlik: " TextStrengthDescriptions="Çok Düşük;Zayıf;Normal;İyi;Çok İyi" CalculationWeightings="50;15;15;20" DisplayPosition="RightSide" runat="server" StrengthIndicatorType="Text" PreferredPasswordLength="8"/>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FtPassword" runat="server" TargetControlID="TxtPassword" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" "/>
                                        <asp:RegularExpressionValidator ID="RePassword" ControlToValidate="TxtPassword" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifreniz en az 1 büyük harf ve 1 rakam içermelidir.  </b></div>" ValidationExpression="((?=.*\d)(?=.*[A-Z_ĞÜŞİÖÇ]).{2,20})" ValidationGroup="UpdatePassword" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="TxtPasswordAgain" runat="server" TextMode="Password" placeholder="Şifre Tekrar" class="form-control" MaxLength="20" minlength="8" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RqPasswordAgain" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Şifreyi tekrar girmelisiniz!</b></div>" ValidationGroup="UpdatePassword" ControlToValidate="TxtPasswordAgain" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CvPasswordAgain" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Yazdığınız şifreler birbirini tutmuyor!</b></div>" ValidationGroup="UpdatePassword" ControlToValidate="TxtPasswordAgain" ControlToCompare="TxtPassword" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:CompareValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FtPasswordAgain" runat="server" TargetControlID="TxtPasswordAgain" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" "/>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <asp:Button ID="BtnUpdatePassword" runat="server" Text="Şifremi Değiştir" class="btn btn-bilgideposu" OnClick="BtnUpdatePassword_Click" ValidationGroup="UpdatePassword"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </asp:Panel>
                </div>
            </div>
            <footer class="footer footer-transparent">
                <div class="container">
                    <p class="copyright pull-right" style="color: white;">
                        &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                    </p>
                </div>
            </footer>
        </div>
    </div>
    <asp:Panel ID="PnlUpdateSuccess" runat="server" Visible="false">
        <div class="sweet-container">
            <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
            <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                <div class="icon error" style="display: none;">
                    <span class="x-mark">
                        <span class="line left"></span>
                        <span class="line right"></span>
                    </span>
                </div>
                <div class="icon warning" style="display: none;">
                    <span class="body"></span><span class="dot"></span>
                </div>
                <div class="icon info" style="display: none;"></div>
                <div class="icon success animate" style="display: block;">
                    <span class="line tip animate-success-tip"></span>
                    <span class="line long animate-success-long"></span>
                    <div class="placeholder"></div>
                    <div class="fix"></div>
                </div>
                <img class="sweet-image" style="display: none;">
                <h4>Tebrikler</h4>
                <div class="sweet-content">
                    <p style="color: green; font-size: 16px;">
                        Şifreniz Değiştirildi<br/>
                        <span style="color: #1C466C; font-size: 13px;">
                            <br/>
                            Şifreniz başarılı bir şekilde değiştirildi.
                        </span>
                    </p>
                </div>
                <hr class="sweet-spacer" style="display: block;">
                <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Sayfası</a><br/>
            </div>
        </div>
    </asp:Panel>
</form>
</body>
<uc1:Footer runat="server" ID="Footer" />
<script type="text/javascript">
    $().ready(function() {

        $('#registerFormValidation').validate();
        $('#loginFormValidation').validate();
        $('#allInputsFormValidation').validate();
    });
</script>
<script type="text/javascript">
    $().ready(function() {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function() {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
                $('#logo').removeClass('card-hidden');
            },
            700);
    });
</script>
</html>