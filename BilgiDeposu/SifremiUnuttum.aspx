﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SifremiUnuttum.aspx.cs" Inherits="BilgiDeposu.SifremiUnuttum" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - Şifremi Unuttum</title>
    <uc1:Head runat="server" ID="Head" />
</head>
<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
    <form id="allInputsFormValidation" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <nav class="navbar navbar-transparent navbar-absolute" style="background-color: transparent;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="KayitOl.aspx">Kayıt Ol
                            </a>
                        </li>
                        <li>
                            <a href="Giris.aspx">Giriş Yap
                            </a>
                        </li>
                        <li>
                            <a href="HesapAktivasyonu.aspx">Hesap Aktivasyonu
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="wrapper wrapper-full-page">
            <div class="full-page login-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg">
                <!-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <asp:Panel ID="PnlForm" runat="server">
                                <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" style="margin-top: -40px;">
                                    <div id="logo" class="card card-hidden" style="background-color: transparent; background-image: url(assets/img/logo-1.png); box-shadow: 0 0px 0px 0px white; height: 118px; margin-left: -5px; margin-top: -20px; position: absolute; width: 118px; z-index: 9999;"></div>
                                    <!-- if you want to have the card without animation please remove the ".card-hidden" class -->
                                    <div class="card card-hidden">
                                        <div class="header text-right" style="margin-right: 40px;">Şifre Al</div>
                                        <div class="content">
                                            <div class="form-group">
                                                <asp:TextBox ID="TxtEmail" runat="server" class="form-control" placeholder="Email Adresiniz" MaxLength="100" ToolTip=" " ValidationGroup="ForgotPassword" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RqEmail" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Email boş bırakılamaz!</b></div>" ValidationGroup="ForgotPassword" ControlToValidate="TxtEmail" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="ReEmail" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Email adresi geçersiz!</b></div>" ValidationGroup="ForgotPassword" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="TxtEmail" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FtEmail" runat="server" TargetControlID="TxtEmail" FilterMode="InvalidChars" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters " InvalidChars=" " />
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox ID="TxtCaptcha" runat="server" class="form-control" MaxLength="15" placeholder="Aşağıdaki Kodu Giriniz" ValidationGroup="ForgotPassword" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RqCaphta" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Doğrulama Kodu Boş Bırakılamaz!</b></div>" ValidationGroup="ForgotPassword" ControlToValidate="TxtCaptcha" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CvCaphta" runat="server" ErrorMessage="<div style='margin-top:10px !important;'><img src='assets/img/hata.png' width='24px' height='24px'><b> Doğrulama Kodu Yanlış!</b></div>" ControlToValidate="TxtCaptcha" OnServerValidate="CaptchaControl" ValidationGroup="ForgotPassword" Font-Size="10pt" ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FtCaptcha" runat="server" TargetControlID="TxtCaptcha" FilterType="Numbers" />

                                            </div>
                                            <div>
                                                <div style="-moz-border-radius: 5px; background-image: url(assets/img/captcha.png); border: dashed #CCC 1px; border-radius: 5px; float: left; height: 40px; margin-left: 27%; margin-right: 10px; width: 100px;"></div>
                                                <input type="button" id="Refresh" onclick="window.location.reload()" runat="server" title="Kodu Yenile" style="background-color: transparent; background-image: url('assets/img/yenile.png'); background-repeat: no-repeat; border: none; height: 40px; width: 13%;" />
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <asp:Button ID="BtnForgotPassword" runat="server" Text="Yeni Şifre Al" class="btn btn-bilgideposu" OnClick="BtnForgotPassword_Click" ValidationGroup="ForgotPassword" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <footer class="footer footer-transparent">
                    <div class="container">
                        <p class="copyright pull-right" style="color: white;">
                            &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                        </p>
                    </div>
                </footer>
            </div>
        </div>

        <asp:Panel ID="PnlInputError" runat="server" Visible="false">
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-danger alert-with-icon animated fadeInDown" role="alert" data-notify-position="bottom-center" style="bottom: 20px; display: inline-block; left: 0px; margin: 0px auto; position: fixed; right: 0px; transition: all 0.5s ease-in-out; z-index: 1031;">
                <span data-notify="icon" class="pe-7s-attention"></span><span data-notify="title"></span><span data-notify="message"><b>Hata</b> - Lütfen Belirtilen Karakterler Arasında Karakter Giriniz !</span><a href="#" target="_blank" data-notify="url"></a>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlAgain" runat="server" Visible="false">
            <div class="sweet-container">
                <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                    <div class="icon error animate">
                        <span class="x-mark">
                            <span class="line left"></span>
                            <span class="line right"></span>
                        </span>
                    </div>
                    <div class="icon danger" style="display: none;">
                        <span class="body"></span><span class="dot"></span>
                    </div>
                    <div class="icon info" style="display: none;"></div>
                    <div class="icon danger" style="display: none;">
                        <span class="line tip animate-error-tip"></span>
                        <span class="line long animate-eroor-long"></span>
                        <div class="placeholder"></div>
                        <div class="fix"></div>
                    </div>
                    <img class="sweet-image" style="display: none;">
                    <h4>Hata!</h4>
                    <div class="sweet-content">
                        <p>
                            <span style="color: red; font-size: 13px;">
                                <b>
                                    <asp:Label ID="LblMail" runat="server" Text=""></asp:Label>
                                </b>adresine <b>
                                    <asp:Label ID="LblMinute" runat="server" Text=""></asp:Label>
                                </b>dakika önce şifre yenileme maili gönderilmiştir!
                            </span>
                        </p>
                        <p>
                            <span style="color: #1C466C; font-size: 13px;">Diğer şifre yenileme maili için <b>
                                <asp:Label ID="LblDiffMinute" runat="server" Text=""></asp:Label>
                            </b>dakika beklemelisiniz!
                            </span>
                        </p>
                    </div>
                    <hr class="sweet-spacer" style="display: block;">
                    <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Yap</a><br />
                    <a class="btn btn-bilgideposu" style="width: 200px;" href="SifremiUnuttum.aspx">Farklı Hesap İçin Gönder</a>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlEmailNotFound" runat="server" Visible="false">
            <div class="sweet-container">
                <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                    <div class="icon error animate">
                        <span class="x-mark">
                            <span class="line left"></span>
                            <span class="line right"></span>
                        </span>
                    </div>
                    <div class="icon danger" style="display: none;">
                        <span class="body"></span><span class="dot"></span>
                    </div>
                    <div class="icon info" style="display: none;"></div>
                    <div class="icon danger" style="display: none;">
                        <span class="line tip animate-error-tip"></span>
                        <span class="line long animate-eroor-long"></span>
                        <div class="placeholder"></div>
                        <div class="fix"></div>
                    </div>
                    <img class="sweet-image" style="display: none;">
                    <h4>Hata!</h4>
                    <div class="sweet-content">
                        <p>
                            <span style="color: #1C466C; font-size: 13px;">Sistemimizde böyle bir mail adresi bulunmamaktadır!</span>
                        </p>
                    </div>
                    <hr class="sweet-spacer" style="display: block;">
                    <a class="btn btn-bilgideposu" href="SifremiUnuttum.aspx" style="margin-bottom: 10px; width: 200px;">Tekrar Dene</a><br />
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlSuccess" runat="server" Visible="false">
            <div class="sweet-container">
                <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                    <div class="icon error" style="display: none;">
                        <span class="x-mark">
                            <span class="line left"></span>
                            <span class="line right"></span>
                        </span>
                    </div>
                    <div class="icon warning" style="display: none;">
                        <span class="body"></span><span class="dot"></span>
                    </div>
                    <div class="icon info" style="display: none;"></div>
                    <div class="icon success animate" style="display: block;">
                        <span class="line tip animate-success-tip"></span>
                        <span class="line long animate-success-long"></span>
                        <div class="placeholder"></div>
                        <div class="fix"></div>
                    </div>
                    <img class="sweet-image" style="display: none;">
                    <h4>Mail Gönderildi</h4>
                    <div class="sweet-content">
                        <p style="color: green; font-size: 13px;">Şifrenizi yenilemeniz için mail adresinize gerekli bilgiler gönderilmiştir.</p>
                    </div>
                    <hr class="sweet-spacer" style="display: block;">
                    <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Yap</a><br />
                    <a class="btn btn-bilgideposu" style="width: 200px;" href="SifremiUnuttum.aspx">Farklı Hesap İçin Gönder</a>
                </div>
            </div>
        </asp:Panel>
    </form>
</body>
<uc1:Footer runat="server" ID="Footer" />
<script type="text/javascript">
    $().ready(function () {

        $('#registerFormValidation').validate();
        $('#loginFormValidation').validate();
        $('#allInputsFormValidation').validate();
    });
</script>
<script type="text/javascript">
    $().ready(function () {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
            $('#logo').removeClass('card-hidden');
        },
            700);
    });
</script>
</html>
