﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;

namespace BilgiDeposu
{
    public partial class HesabiDogrula : Connect
    {
        private string Email, Pin;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Code"] != null && Request.QueryString["Code"].Length >= 15 &&
                Request.QueryString["Verification"] != null && Request.QueryString["Verification"].Length >= 8)
            {
                try
                {
                    Email = Encryption.Decode(Encryption.Decode(Request.QueryString["Code"]));
                    Pin = Encryption.Decode(Encryption.Decode(Request.QueryString["Verification"]));
                }
                catch
                {
                    Response.Redirect("404.aspx");
                }

                var emailControl = db.User.FirstOrDefault(k => k.Email == Email);
                if (emailControl == null)
                {
                    Response.Redirect("404.aspx");
                }
                else
                {
                    var emailVerificationControl = db.User.FirstOrDefault(k => k.Email == Email && k.EmailVerification);

                    if (emailVerificationControl != null)
                    {
                        PnlVericationSuccess.Visible = false;
                        PnlAlreadyActive.Visible = true;
                    }
                    else
                    {
                        var pinControl =
                            db.LastRequest.FirstOrDefault(
                                k => k.User.Email == Email && k.Process == LastRequestType.MailActivation);


                        if (pinControl.VerificationCode == Pin)
                        {
                            var emailVerificationUpdate = db.User.FirstOrDefault(k => k.Email == Email);
                            emailVerificationUpdate.EmailVerification = true;
                            db.SaveChanges();
                            PnlVericationSuccess.Visible = true;
                            PnlAlreadyActive.Visible = false;
                        }
                        else
                        {
                            Response.Redirect("404.aspx");
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }
}