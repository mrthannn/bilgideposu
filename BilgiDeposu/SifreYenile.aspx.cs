﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using BilgiDeposu.Class;
using BilgiDeposu.Enums;
using BilgiDeposu.Methods;

namespace BilgiDeposu
{
    public partial class SifreYenile : Connect
    {
        string Email, Pin;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Code"] != null && Request.QueryString["Code"].Length >= 15 && Request.QueryString["Verification"] != null && Request.QueryString["Verification"].Length >= 8)
            {
                try
                {
                    Email = Encryption.Decode(Encryption.Decode(Request.QueryString["Code"]));
                    Pin = Encryption.Decode(Encryption.Decode(Request.QueryString["Verification"]));
                }
                catch
                {
                    Response.Redirect("404.aspx");
                }

                var emailControl = db.User.FirstOrDefault(k => k.Email == Email);

                if (emailControl == null)
                {
                    Response.Redirect("404.aspx");
                }
                else
                {
                    var pinControl =
                        db.LastRequest.FirstOrDefault(k => k.User.Email == Email &&
                                                           k.Process == LastRequestType.ForgotPassword);

                    if (pinControl != null)
                    {
                        if (Pin == pinControl.VerificationCode)
                        {
                            LblTitle.Text = "<b>Email : </b>" + Email;
                        }
                        else
                        {
                            Response.Redirect("404.aspx");
                        }

                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }

        protected void BtnUpdatePassword_Click(object sender, EventArgs e)
        {
            var sifre=TxtPassword.Text.Trim();

            var updatePassword = db.User.FirstOrDefault(k => k.Email==this.Email);
            updatePassword.Password = Encryption.Sha256Code(sifre);
            
            var pinControl =
                db.LastRequest.FirstOrDefault(k => k.User.Email == Email &&
                                                   k.Process == LastRequestType.ForgotPassword);
            pinControl.VerificationCode = Encryption.GenerateCode();
            db.SaveChanges();
            PnlUpdateSuccess.Visible = true;
            PnlForm.Visible = false;


        }
    }
}