﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BilgiDeposu.Methods
{
    public class Encryption
    {
        public static string GenerateCode()
        {
            Random r = new Random();
            string sayi = (r.Next(1000, 99999)).ToString();
            return sayi;
        }

        public static string Sha256Code(string data)
        {
            SHA256 sha = new SHA256Managed();
            byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));
            StringBuilder stringBuilder = new StringBuilder();
            foreach (byte b in hash)
            {
                stringBuilder.AppendFormat("{0:x2}", b);
            }
            return stringBuilder.ToString();
        }

        public static string Encode(string data)
        {
            byte[] sifreli = Encoding.ASCII.GetBytes(data.ToString());

            StringBuilder kod = new StringBuilder();
            for (int i = 0; i < sifreli.Length; i++)
            {

                kod.Append(string.Format("{0:x}", sifreli[i] + 3 * i + 5));
            }
            return kod.ToString();
        }

        public static string Decode(string data)
        {
            byte[] sifreli = new byte[data.Length / 2];
            int sayac = 0;
            for (int i = 1; i < data.Length; i += 2)
            {
                sifreli[sayac] = Convert.ToByte(Convert.ToInt64(data.Substring(i - 1, 2), 16) - 3 * sayac - 5);
                sayac++;
            }
            char[] a = Encoding.ASCII.GetChars(sifreli);
            data = "";
            for (int i = 0; i < a.Length; i++)
            {
                data += a[i];
            }
            return data;
        }
    }
}