﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HesabiDogrula.aspx.cs" Inherits="BilgiDeposu.HesabiDogrula" %>

<%@ Register Src="~/UserControl/LoginAndRegister/Head.ascx" TagPrefix="uc1" TagName="Head" %>
<%@ Register Src="~/UserControl/LoginAndRegister/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bilgi Deposu - Hesabı Doğrula</title>
    <uc1:Head runat="server" ID="Head"/>
</head>
<body style="overflow-x: hidden;" class="scrollbar" id="scroll">
<form id="allInputsFormValidation" runat="server">
    <nav class="navbar navbar-transparent navbar-absolute" style="background-color: transparent;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="KayitOl.aspx">
                            Kayıt Ol
                        </a>
                    </li>
                    <li>
                        <a href="Giris.aspx">
                            Giriş Yap
                        </a>
                    </li>
                    <li>
                        <a href="HesapAktivasyonu.aspx">
                            Hesap Aktivasyonu
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="black" data-image="assets/img/full-screen-image-3.jpg">

            <!-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" style="margin-top: -40px;">

                            <asp:Panel ID="PnlVericationSuccess" runat="server" Visible="false">
                                <div class="sweet-container">
                                    <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                                    <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                                        <div class="icon error" style="display: none;">
                                            <span class="x-mark">
                                                <span class="line left"></span>
                                                <span class="line right"></span>
                                            </span>
                                        </div>
                                        <div class="icon warning" style="display: none;">
                                            <span class="body"></span><span class="dot"></span>
                                        </div>
                                        <div class="icon info" style="display: none;"></div>
                                        <div class="icon success animate" style="display: block;">
                                            <span class="line tip animate-success-tip"></span>
                                            <span class="line long animate-success-long"></span>
                                            <div class="placeholder"></div>
                                            <div class="fix"></div>
                                        </div>
                                        <img class="sweet-image" style="display: none;">
                                        <h4>Tebrikler</h4>
                                        <div class="sweet-content">
                                            <p style="color: green; font-size: 16px;">
                                                Hesabınız Doğrulandı.<br/>
                                                <span style="color: #1C466C; font-size: 13px;">
                                                    <br/>
                                                    Yönetici Hesabınıza İzin Verdikten Sonra Akademisyenlerin İzin Verdiği Derslere Erişebilirsiniz!
                                                </span>
                                            </p>
                                        </div>
                                        <hr class="sweet-spacer" style="display: block;">
                                        <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Sayfası</a><br/>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="PnlAlreadyActive" runat="server" Visible="false">
                                <div class="sweet-container">
                                    <div class="sweet-overlay" tabindex="-1" style="display: block; opacity: 1.16;"></div>
                                    <div class="sweet-alert show-sweet-alert visible" style="background: rgb(255, 255, 255); display: block; margin-left: -155px; margin-top: -210px; padding: 20px; width: 310px;" tabindex="-1">
                                        <div class="icon error animate">
                                            <span class="x-mark">
                                                <span class="line left"></span>
                                                <span class="line right"></span>
                                            </span>
                                        </div>
                                        <div class="icon danger" style="display: none;">
                                            <span class="body"></span><span class="dot"></span>
                                        </div>
                                        <div class="icon info" style="display: none;"></div>
                                        <div class="icon danger" style="display: none;">
                                            <span class="line tip animate-error-tip"></span>
                                            <span class="line long animate-eroor-long"></span>
                                            <div class="placeholder"></div>
                                            <div class="fix"></div>
                                        </div>
                                        <img class="sweet-image" style="display: none;">
                                        <h4>Hata!</h4>
                                        <div class="sweet-content">
                                            <p>
                                                <span style="color: #1C466C; font-size: 13px;">Bu mail adresi zaten doğrulanmış!</span>
                                            </p>
                                        </div>
                                        <hr class="sweet-spacer" style="display: block;">
                                        <a class="btn btn-bilgideposu" href="Giris.aspx" style="margin-bottom: 10px; width: 200px;">Giriş Yap</a><br/>
                                        <a class="btn btn-bilgideposu" style="width: 200px;" href="HesapAktivasyonu.aspx">Farklı Hesabı Doğrula</a>
                                    </div>
                                </div>
                            </asp:Panel>

                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer footer-transparent">
                <div class="container">
                    <p class="copyright pull-right" style="color: white;">
                        &copy; 2017 | By <a style="font-weight: bold;" href="http://www.hanmurat.com">Murat HAN</a> | Bilgi Deposu
                    </p>
                </div>
            </footer>
        </div>
    </div>
</form>
</body>
<uc1:Footer runat="server" ID="Footer"/>
<script type="text/javascript">
    $().ready(function() {

        $('#registerFormValidation').validate();
        $('#loginFormValidation').validate();
        $('#allInputsFormValidation').validate();
    });
</script>
<script type="text/javascript">
    $().ready(function() {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function() {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            },
            700);
    });
</script>
</html>